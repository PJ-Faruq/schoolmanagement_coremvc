﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SchoolManagementSystem.ViewModel;
using SchoolManagementSystem.Models;

namespace SchoolManagementSystem.Models
{
    public class SchoolDbContext : IdentityDbContext
    {
        public SchoolDbContext(DbContextOptions<SchoolDbContext> options) : base(options) { }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


            //Remove Foreign Key Restriction
            foreach (var foreignKey in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                foreignKey.DeleteBehavior = DeleteBehavior.Restrict;
            }

            // Extend models to include a LastUpdated date shadow property
            modelBuilder.Entity<Student>()
                .Property<DateTime>("LastUpdated");
            modelBuilder.Entity<Programme>()
                .Property<DateTime>("LastUpdated");
            modelBuilder.Entity<Module>()
                .Property<DateTime>("LastUpdated");
            modelBuilder.Entity<Assessment>()
                .Property<DateTime>("LastUpdated");
            modelBuilder.Entity<AssessmentResult>()
                .Property<DateTime>("LastUpdated");


            //Avoiding Cycles

            //modelBuilder.Entity<Assessment>()
            //    .HasOne<Module>(sc => sc.Module)
            //    .WithMany(s => s.Assessments)
            //    .HasForeignKey(sc => sc.ModuleID);


           // modelBuilder.Entity<AssessmentResult>()   
           //.HasOne(u => u.Module).WithMany(u => u.AssessmentResults).IsRequired().OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<AssessmentResult>()
                .HasOne<Module>(sc => sc.Module)
                .WithMany(s => s.AssessmentResults)
                .HasForeignKey(sc => sc.ModuleID);
        }

        public DbSet<AppUser> AppUsers { get; set; }
        public DbSet<Student> Students { get; set; }

        public DbSet<Programme> Programmes { get; set; }

        public DbSet<Module> Modules { get; set; }

       // public DbSet<ProgrammeModule> ProgrammeModules { get; set; }

        public DbSet<Assessment> Assessments { get; set; }

        public DbSet<AssessmentResult> AssessmentResults { get; set; }

        public DbSet<SchoolManagementSystem.ViewModel.LoginVm> LoginVm { get; set; }

        public DbSet<SchoolManagementSystem.Models.Teacher> Teachers { get; set; }

    }
}
