﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SchoolManagementSystem.Models;

namespace SchoolManagementSystem.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ModuleController : Controller
    {
        private readonly SchoolDbContext _context;

        public ModuleController(SchoolDbContext context)
        {
            _context = context;
        }

        // GET: Module
        public async Task<IActionResult> Index()
        {
            return View(await _context.Modules.Include(m=>m.Programme).ToListAsync());
        }

        // GET: Module/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @module = await _context.Modules
                .FirstOrDefaultAsync(m => m.ModuleID == id);
            if (@module == null)
            {
                return NotFound();
            }

            return View(@module);
        }

        // GET: Module/Create
        public IActionResult Create()
        {
            try
            {
                ViewBag.ProgrammeList = _context.Programmes.ToList();
                ViewBag.ModuleID = GetModuleId();
                return View();
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }

            
        }

        // POST: Module/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Module @module)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Add(@module);
                    await _context.SaveChangesAsync();
                    ViewBag.SuccessMsg = "Module Created Successfully";

                    ViewBag.ProgrammeList = _context.Programmes.ToList();
                    ViewBag.ModuleID = GetModuleId();
                    ModelState.Clear();
                    return View();
                }
                return View(@module);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }
            
        }


        private string GetModuleId()
        {
            string moduleID = "";
            while (_context.Modules.Any(m => m.ModuleID == moduleID) || moduleID == "")
            {
                Random r = new Random();
                moduleID = "M" + r.Next(100_00, 999_99).ToString();
            }
            return moduleID;
        }

        // GET: Module/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @module = await _context.Modules.FindAsync(id);
            ViewBag.ProgrammeList = _context.Programmes.ToList();
            //ViewBag.ProgrammeID= new SelectList(_context.Programmes, "Id", "Name", module.ProgrammeID);
            if (@module == null)
            {
                return NotFound();
            }
            return View(@module);
        }

        // POST: Module/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("ModuleID,ModuleName,ModuleDescription,ModuleCredits")] Module @module)
        {
            if (id != @module.ModuleID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(@module);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ModuleExists(@module.ModuleID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(@module);
        }

        // GET: Module/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            var @module = await _context.Modules
                .FirstOrDefaultAsync(m => m.ModuleID == id);
            if (@module == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            return View(@module);
        }

        // POST: Module/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            try
            {
                var @module = await _context.Modules.FindAsync(id);
                _context.Modules.Remove(@module);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateException)
            {
                string errorMsg = "Module cannot be deleted. Because It is used in other Entity (Ex. Student, Assessment, AssessmentResult). If you really want to Delete, Please remove those entity first which use this Module and Try Again";
                return RedirectToAction("Delete", "Error", new { errorMsg = errorMsg });
            }


        }

        private bool ModuleExists(string id)
        {
            return _context.Modules.Any(e => e.ModuleID == id);
        }
    }
}
