﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SchoolManagementSystem.Models;

namespace SchoolManagementSystem.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AssessmentsController : Controller
    {
        private readonly SchoolDbContext _context;

        public AssessmentsController(SchoolDbContext context)
        {
            _context = context;
        }

        // GET: Assessments
        public async Task<IActionResult> Index()
        {
            return View(await _context.Assessments.Include(m=>m.Module).ToListAsync());
        }

        // GET: Assessments/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var assessment = await _context.Assessments
                .FirstOrDefaultAsync(m => m.AssessmentID == id);
            if (assessment == null)
            {
                return NotFound();
            }

            return View(assessment);
        }

        // GET: Assessments/Create
        public IActionResult Create()
        {
            try
            {
                ViewBag.ModuleList = _context.Modules.ToList();
                ViewBag.AssessmentID = GetAssessmentID();
                return View();
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }
            
        }

        private string GetAssessmentID()
        {
            string assessmentID = "";
            while (_context.Assessments.Any(m => m.AssessmentID == assessmentID) || assessmentID == "")
            {
                Random r = new Random();
                assessmentID = "M" + r.Next(100_00, 999_99).ToString();
            }
            return assessmentID;
        }

        // POST: Assessments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create( Assessment assessment)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Add(assessment);
                    await _context.SaveChangesAsync();

                    ViewBag.SuccessMsg = "Assessment Created Successfully";
                    ViewBag.ModuleList = _context.Modules.ToList();
                    ViewBag.AssessmentID = GetAssessmentID();
                    ModelState.Clear();
                    return View();
                }
                ViewBag.ModuleList = _context.Modules.ToList();
                return View(assessment);
            }
            catch (Exception)
            {
                return RedirectToAction("Unknown", "Error");
            }

            
        }

        // GET: Assessments/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            ViewBag.ModuleList = _context.Modules.ToList();
            var assessment = await _context.Assessments.FindAsync(id);
            if (assessment == null)
            {
                return NotFound();
            }
            return View(assessment);
        }

        // POST: Assessments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("AssessmentID,AssessmentName,AssessmentDescription,AssessmentTotalMark,AssessmentType,ModuleID")] Assessment assessment)
        {
            if (id != assessment.AssessmentID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(assessment);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AssessmentExists(assessment.AssessmentID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(assessment);
        }

        // GET: Assessments/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            var assessment = await _context.Assessments
                .FirstOrDefaultAsync(m => m.AssessmentID == id);
            if (assessment == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            return View(assessment);
        }

        // POST: Assessments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            try
            {
                var assessment = await _context.Assessments.FindAsync(id);
                _context.Assessments.Remove(assessment);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateException)
            {

                string errorMsg = "Assessment cannot be deleted. Because It is used in other Entity (Ex. AssessmentResult). If you really want to Delete, Please remove those entity first which use this Assessment and Try Again";
                return RedirectToAction("Delete", "Error", new { errorMsg = errorMsg });
            }

            
        }

        private bool AssessmentExists(string id)
        {
            return _context.Assessments.Any(e => e.AssessmentID == id);
        }
    }
}
