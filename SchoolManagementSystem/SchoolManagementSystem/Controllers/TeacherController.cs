﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SchoolManagementSystem.Models;
using SchoolManagementSystem.ViewModel;

namespace SchoolManagementSystem.Controllers
{
    [Authorize]
    public class TeacherController : Controller
    {
        private readonly SchoolDbContext _context;
        private readonly UserManager<AppUser> userManager;

        public TeacherController(SchoolDbContext context, UserManager<AppUser> userManager)
        {
            _context = context;
            this.userManager = userManager;
        }

        [Authorize(Roles = "Admin")]
        // GET: Admin/RegisterTeacher
        public IActionResult Register()
        {
            try
            {
                ViewBag.ProgrammeList = ViewBag.ProgrammeList = _context.Programmes.ToList();
                ViewBag.TeacherId = GetTeacherId();
                return View();
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }

        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        // GET: Admin/RegisterStudent
        public async Task<IActionResult> Register(Teacher teacher, IFormFile image)
        {

            try
            {
                if (image != null)
                {
                    using (var stream = new MemoryStream())
                    {
                        image.CopyTo(stream);
                        teacher.TeacherImage = stream.ToArray();
                    }
                }

                if (ModelState.IsValid)
                {
                    _context.Add(teacher);
                    bool status = _context.SaveChanges() > 0;

                    //Create Teacher Role for Authentication
                    if (status)
                    {
                        string fullName = teacher.FirstName + " " + teacher.SurName;
                        await CreateTeacherAsUserAsync(teacher.TeacherID, teacher.Password,fullName);
                    }

                    ViewBag.ProgrammeList = _context.Programmes.ToList();
                    ViewBag.TeacherId = GetTeacherId();
                    ViewBag.SuccessMsg = "Teacher Successfully Registered";


                    ModelState.Clear();
                    return View();
                }

                ViewBag.ProgrammeList = _context.Programmes.ToList();
                ViewBag.TeacherId = GetTeacherId();
                return View(teacher);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }


           
        }

        private async Task CreateTeacherAsUserAsync(string teacherID, string password,string fullName)
        {
            AppUser user = new AppUser();
            user.UserName = teacherID;
            user.FullName = fullName;
            var result = await userManager.CreateAsync(user, password);
            await userManager.AddToRoleAsync(user, "Teacher");
        }

        private string GetTeacherId()
        {
            string teacherId = "";
            while (_context.Teachers.Any(m => m.TeacherID == teacherId) || teacherId == "")
            {
                Random r = new Random();
                teacherId = "T" + r.Next(100_000, 999_999).ToString();
            }
            return teacherId;
        }


        [Authorize(Roles = "Admin")]
        // GET: Teacher
        public async Task<IActionResult> Index()
        {
            try
            {
                var schoolDbContext = _context.Teachers.Include(t => t.Programme);
                return View(await schoolDbContext.ToListAsync());
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }
            
        }

        [Authorize(Roles = "Admin, Teacher")]
        // GET: Teacher/Details/5
        public async Task<IActionResult> Details(string id)
        {
            try
            {
                if (id == null)
                {
                    return RedirectToAction("NotFoundPage", "Error");
                }

                var teacher = await _context.Teachers
                    .Include(t => t.Programme)
                    .FirstOrDefaultAsync(m => m.TeacherID == id);
                if (teacher == null)
                {
                    return RedirectToAction("NotFoundPage", "Error");
                }

                return View(teacher);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }


        }

        [Authorize(Roles = "Admin")]
        // GET: Teacher/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var teacher = await _context.Teachers.FindAsync(id);
            if (teacher == null)
            {
                return NotFound();
            }
            ViewData["ProgrammeID"] = new SelectList(_context.Programmes, "ProgrammeID", "ProgrammeID", teacher.ProgrammeID);
            return View(teacher);
        }

        // POST: Teacher/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("TeacherID,FirstName,SurName,Password,AddressOne,AddressTwo,Town,County,MobilePhoneNumber,EmailAddress,EmergencyMobilePhoneNumber,TeacherPPS,ProgrammeFeePaid,GenderType,FullOrPartTime,TeacherImage,ProgrammeID")] Teacher teacher)
        {
            if (id != teacher.TeacherID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(teacher);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TeacherExists(teacher.TeacherID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProgrammeID"] = new SelectList(_context.Programmes, "ProgrammeID", "ProgrammeID", teacher.ProgrammeID);
            return View(teacher);
        }

        [Authorize(Roles = "Admin")]
        // GET: Teacher/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            try
            {
                if (id == null)
                {
                    return RedirectToAction("NotFoundPage", "Error");
                }

                var teacher = await _context.Teachers
                    .Include(t => t.Programme)
                    .FirstOrDefaultAsync(m => m.TeacherID == id);
                if (teacher == null)
                {
                    return RedirectToAction("NotFoundPage", "Error");
                }

                return View(teacher);
            }
            catch (Exception)
            {

                throw;
            }

           
        }

        [Authorize(Roles = "Admin")]
        // POST: Teacher/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            try
            {
                var teacher = await _context.Teachers.FindAsync(id);
                _context.Teachers.Remove(teacher);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }

            
        }

        private bool TeacherExists(string id)
        {
            return _context.Teachers.Any(e => e.TeacherID == id);
        }
    }
}
