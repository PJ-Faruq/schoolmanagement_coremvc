﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SchoolManagementSystem.Models;

namespace SchoolManagementSystem.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ProgrammeController : Controller
    {
        private readonly SchoolDbContext _context;

        public ProgrammeController(SchoolDbContext context)
        {
            _context = context;
        }

        // GET: Programme
        public async Task<IActionResult> Index()
        {
            return View(await _context.Programmes.ToListAsync());
        }

        // GET: Programme/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var programme = await _context.Programmes
                .FirstOrDefaultAsync(m => m.ProgrammeID == id);
            if (programme == null)
            {
                return NotFound();
            }

            return View(programme);
        }

        // GET: Programme/Create
        public IActionResult Create()
        {
            ViewBag.ProgrammeID = GetProgrammeID();
            return View();
        }


        // POST: Programme/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ProgrammeID,ProgrammeName,ProgrammeDescription,ProgrammeQQILevel,ProgrammeCredits,ProgrammeCost")] Programme programme)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Add(programme);
                    await _context.SaveChangesAsync();
                    ViewBag.SuccessMsg = "Programme Created Successfully";

                    ViewBag.ProgrammeID = GetProgrammeID();
                    ModelState.Clear();
                    return View();
                }
                return View(programme);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }


        }

        private string GetProgrammeID()
        {
            string programmeID = "";
            while (_context.Programmes.Any(m => m.ProgrammeID == programmeID) || programmeID == "")
            {
                Random r = new Random();
                programmeID = "P" + r.Next(100_00, 999_99).ToString();
            }
            return programmeID;
        }

        // GET: Programme/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var programme = await _context.Programmes.FindAsync(id);
            if (programme == null)
            {
                return NotFound();
            }
            return View(programme);
        }

        // POST: Programme/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("ProgrammeID,ProgrammeName,ProgrammeDescription,ProgrammeQQILevel,ProgrammeCredits,ProgrammeCost")] Programme programme)
        {
            if (id != programme.ProgrammeID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(programme);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProgrammeExists(programme.ProgrammeID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(programme);
        }

        // GET: Programme/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            var programme = await _context.Programmes
                .FirstOrDefaultAsync(m => m.ProgrammeID == id);
            if (programme == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            return View(programme);
        }

        // POST: Programme/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            try
            {
                var programme = await _context.Programmes.FindAsync(id);
                _context.Programmes.Remove(programme);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateException )
            {
                string errorMsg = "Programme cannot be deleted. Because It is used in other Entity (Ex. Student, Module, Assessment). If you really want to Delete, Please remove those entity first which use this programme and Try Again";
                return RedirectToAction("Delete", "Error", new { errorMsg = errorMsg });
            }
            
        }

        private bool ProgrammeExists(string id)
        {
            return _context.Programmes.Any(e => e.ProgrammeID == id);
        }
    }
}
