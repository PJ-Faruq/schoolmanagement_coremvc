﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace SchoolManagementSystem.Controllers
{
    public class ErrorController : Controller
    {
        public IActionResult NotFoundPage()
        {
            return View();
        }

        public IActionResult Unauthorize()
        {
            return View();
        }

        public IActionResult Unknown()
        {
            return View();
        }

        public IActionResult Delete(string errorMsg)
        {
            ViewBag.errorMsg = errorMsg;
            return View();
        }

        //public IActionResult Unknown(string errorMsg)
        //{
        //    ViewBag.errorMsg = errorMsg;
        //    return View();
        //}
    }
}