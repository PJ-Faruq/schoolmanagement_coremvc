﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SchoolManagementSystem.Models;
using SchoolManagementSystem.ViewModel;

namespace SchoolManagementSystem.Controllers
{
    [Authorize]
    public class StudentController : Controller
    {
        private readonly SchoolDbContext _context;
        private readonly UserManager<AppUser> userManager;

        public StudentController(SchoolDbContext context, UserManager<AppUser> userManager)
        {
            _context = context;
            this.userManager = userManager;
        }

        [Authorize(Roles = "Admin")]
        // GET: Student/RegisterStudent
        public IActionResult Register()
        {
            try
            {
                ViewBag.ProgrammeList = _context.Programmes.ToList();
                ViewBag.StudentId = GetStudentId();
                return View();
            }
            catch (Exception e)
            {

                //return RedirectToAction("Unknown", "Error",new { errorMsg= e.Message.ToString() } );
                return RedirectToAction("Unknown", "Error");
            }

        }

        private string GetStudentId()
        {
            string studentId = "";
            while (_context.Students.Any(m => m.StudentID == studentId) || studentId == "")
            {
                Random r = new Random();
                studentId = "S" + r.Next(100_000, 999_999).ToString();
            }
            return studentId;
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        // GET: Student/RegisterStudent
        public async Task<IActionResult> Register(Student student, IFormFile image)
        {
            try
            {
                if (image != null)
                {
                    using (var stream = new MemoryStream())
                    {
                        image.CopyTo(stream);
                        student.StudentImage = stream.ToArray();
                    }
                }

                if (ModelState.IsValid)
                {
                    _context.Add(student);
                   bool status = _context.SaveChanges()>0;

                    //Create Student Role for Authentication
                    if (status)
                    {
                        string fullName = student.FirstName + " " + student.SurName;
                        await CreateStudentAsUserAsync(student.StudentID,student.Password, fullName);
                    }

                    ViewBag.ProgrammeList = _context.Programmes.ToList();
                    ViewBag.StudentId = GetStudentId();
                    ViewBag.SuccessMsg = "Student Successfully Registered";


                    ModelState.Clear();
                    return View();
                }

                ViewBag.ProgrammeList = _context.Programmes.ToList();
                ViewBag.StudentId = GetStudentId();
                return View(student);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }


        }

        private async Task CreateStudentAsUserAsync(string studentID, string password ,string fullName)
        {
            AppUser user = new AppUser();
            user.UserName = studentID;
            user.FullName = fullName;
            var result = await userManager.CreateAsync(user, password);
            await userManager.AddToRoleAsync(user, "Student");
        }

        [Authorize(Roles = "Admin")]
        // GET: Student
        public async Task<IActionResult> ShowAll()
        {
            try
            {
                return View(await _context.Students.Include(m => m.Programme).ToListAsync());
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }
            
        }

        [Authorize(Roles = "Admin, Student")]
        //Show result of a particular Student
        public async Task<IActionResult> ShowResult(string id)
        {
            try
            {
                Student std = _context.Students
                .Include(m => m.Programme)
                .FirstOrDefault(m => m.StudentID == id);
                if (std == null)
                {
                    return RedirectToAction("NotFoundPage", "Error");
                }

                var assessmentResultList = await _context.AssessmentResults.Where(m => m.StudentID == id)
                    .Include(a => a.Module)
                    .Include(b => b.Programme)
                    .Include(c => c.Student)
                    .Include(d => d.Assessment)
                    .ToListAsync();

                ShowStudentResultViewModel vm = new ShowStudentResultViewModel();
                vm.StudentName = std.FirstName + " " + std.SurName;
                vm.Programme = std.Programme.ProgrammeName;
                vm.StudentImage = std.StudentImage;
                vm.listOfAssessmentResult = assessmentResultList;


                return View(vm);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }

            
        }

        [Authorize(Roles = "Admin, Student")]
        // GET: Student/Details/5
        public async Task<IActionResult> Details(string id)
        {
            try
            {
                if (id == null)
                {
                    return RedirectToAction("NotFoundPage", "Error");
                }

                var student = await _context.Students
                    .Include(m => m.Programme)
                    .FirstOrDefaultAsync(m => m.StudentID == id);
                if (student == null)
                {
                    return RedirectToAction("NotFoundPage", "Error");
                }

                return View(student);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }


        }

        [Authorize(Roles = "Admin")]
        // GET: Student/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Students.FindAsync(id);
            if (student == null)
            {
                return NotFound();
            }
            return View(student);
        }

        // POST: Student/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("StudentID,FirstName,SurName,Password,AddressOne,AddressTwo,Town,County,MobilePhoneNumber,EmailAddress,EmergencyMobilePhoneNumber,StudentPPS,ProgrammeFeePaid,DateOfBirth,GenderType,FullOrPartTime,StudentImage,ProgrammeID")] Student student)
        {
            if (id != student.StudentID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(student);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StudentExists(student.StudentID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(ShowAll));
            }
            return View(student);
        }

        [Authorize(Roles = "Admin")]
        // GET: Student/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            try
            {
                if (id == null)
                {
                    return RedirectToAction("NotFoundPage", "Error");
                }

                var student = await _context.Students
                    .FirstOrDefaultAsync(m => m.StudentID == id);
                if (student == null)
                {
                    return RedirectToAction("NotFoundPage", "Error");
                }

                return View(student);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }
            
        }

        [Authorize(Roles = "Admin")]
        // POST: Student/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            try
            {
                var student = await _context.Students.FindAsync(id);
                _context.Students.Remove(student);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(ShowAll));
            }
            catch (DbUpdateException)
            {

                string errorMsg = "Student cannot be deleted. Because It is used in other Entity (Ex. AssessmentResult). If you really want to Delete, Please remove those entity first which use this Student and Try Again";
                return RedirectToAction("Delete", "Error", new { errorMsg = errorMsg });
            }
            
        }

        private bool StudentExists(string id)
        {
            return _context.Students.Any(e => e.StudentID == id);
        }
    }
}
