﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SchoolManagementSystem.Models;
using SchoolManagementSystem.ViewModel;

namespace SchoolManagementSystem.Controllers
{
    [Authorize]
    public class AssessmentResultsController : Controller
    {
        private readonly SchoolDbContext _context;

        public AssessmentResultsController(SchoolDbContext context)
        {
            _context = context;
        }

        [Authorize(Roles = "Admin, Teacher")]
        // GET: AssessmentResults/Create
        public IActionResult Submit(string id)
        {
            try
            {
                Student std = _context.Students
                            .Include(m => m.Programme)
                            .FirstOrDefault(m => m.StudentID == id);
                if (std == null)
                {
                    ViewBag.StudentList = _context.Students.ToList();
                }
                else
                {
                    ViewBag.StudentId = std.StudentID;
                    ViewBag.StudentFullName = std.FirstName + " " + std.SurName;
                }


                return View();
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }


        }


        [Authorize(Roles = "Admin, Teacher")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Submit(AssessmentResult assessmentResult)
        {
            try
            {
                string assessmentResultId = GetAssessmentResultId();
                assessmentResult.AssessmentResultID = assessmentResultId;
                if (ModelState.IsValid)
                {
                    _context.Add(assessmentResult);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(ShowAll));
                }

                Student std = _context.Students
                                .Include(m => m.Programme)
                                .FirstOrDefault(m => m.StudentID == assessmentResult.StudentID);
                if (std == null)
                {
                    ViewBag.StudentList = _context.Students.ToList();
                }
                else
                {
                    ViewBag.StudentId = std.StudentID;
                    ViewBag.StudentFullName = std.FirstName + " " + std.SurName;
                }

                return View(assessmentResult);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }

           
        }

        private string GetAssessmentResultId()
        {
            string studentID = "";
            while (_context.AssessmentResults.Any(m => m.StudentID == studentID) || studentID == "")
            {
                Random r = new Random();
                studentID = "AsRes" + r.Next(100_000, 999_999).ToString();
            }
            return studentID;
        }


        public JsonResult GetProgrammeIdByStudent(string id)
        {
            var student = _context.Students.Include(c => c.Programme).FirstOrDefault(m => m.StudentID == id);
            //var json = JsonConvert.SerializeObject(student);
            return Json(student);
        }

        public JsonResult GetModuleByProgramme(string id)
        {
            var moduleList = _context.Modules.Where(m => m.ProgrammeID == id).ToList();
            //var json = JsonConvert.SerializeObject(student);
            return Json(moduleList);
        }


        public JsonResult GetAssessmentByModule(string id)
        {
            var moduleList = _context.Assessments.Where(m => m.ModuleID == id).ToList();
            //var json = JsonConvert.SerializeObject(student);
            return Json(moduleList);
        }

        public JsonResult GetStudentById(string id)
        {
            Student student = _context.Students.FirstOrDefault(m => m.StudentID == id);

            string fullName = "";
            if (student != null)
            {
                fullName= student.FirstName + " " + student.SurName;
            }
            
            //var json = JsonConvert.SerializeObject(student);
            return Json(fullName);
        }


        [Authorize(Roles = "Admin, Teacher")]
        public async Task<IActionResult> ShowAll()
        {
            ViewBag.programmeList = _context.Programmes.ToList();
            var AssessResultList = await _context.AssessmentResults
                .Include(a => a.Module)
                .Include(b => b.Programme)
                .Include(c => c.Student)
                .Include(d => d.Assessment)
                .ToListAsync();
            return View(AssessResultList);
        }


        [Authorize(Roles = "Admin, Teacher")]
        [HttpPost]
        public async Task<IActionResult> ShowAll(string ProgrammeID)
        {
            try
            {
                ViewBag.programmeList = _context.Programmes.ToList();
                var AssessResultList = await _context.AssessmentResults.Where(m => m.ProgrammeID == ProgrammeID)
                    .Include(a => a.Module)
                    .Include(b => b.Programme)
                    .Include(c => c.Student)
                    .Include(d => d.Assessment)
                    .ToListAsync();
                return View(AssessResultList);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }

            
        }


        [Authorize(Roles = "Admin, Teacher")]
        public IActionResult ShowAssessmentResult(string id)
        {
            try
            {
                var AssessResult = _context.AssessmentResults
                .Include(a => a.Module)
                .Include(b => b.Programme)
                .Include(c => c.Student)
                .Include(d => d.Assessment).FirstOrDefault(m => m.AssessmentResultID == id);

                return View(AssessResult);
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }
            
        }


        // GET: AssessmentResults/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var assessmentResult = await _context.AssessmentResults
                .FirstOrDefaultAsync(m => m.AssessmentResultID == id);
            if (assessmentResult == null)
            {
                return NotFound();
            }

            return View(assessmentResult);
        }


        // GET: AssessmentResults/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var assessmentResult = await _context.AssessmentResults.FindAsync(id);
            if (assessmentResult == null)
            {
                return NotFound();
            }
            return View(assessmentResult);
        }

        // POST: AssessmentResults/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("AssessmentResultID,AssessmentResultDescription,AssessmentResultMark,StudentID,ProgrammeID,AssessmentDate,ModuleID,AssessmentID")] AssessmentResult assessmentResult)
        {
            if (id != assessmentResult.AssessmentResultID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(assessmentResult);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AssessmentResultExists(assessmentResult.AssessmentResultID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(ShowAll));
            }
            return View(assessmentResult);
        }


        [Authorize(Roles = "Admin")]
        // GET: AssessmentResults/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            var assessmentResult = await _context.AssessmentResults
                .Include(a => a.Module)
                .Include(b => b.Programme)
                .Include(c => c.Student)
                .Include(d => d.Assessment)
                .FirstOrDefaultAsync(m => m.AssessmentResultID == id);
            if (assessmentResult == null)
            {
                return RedirectToAction("NotFoundPage", "Error");
            }

            return View(assessmentResult);
        }

        [Authorize(Roles = "Admin")]
        // POST: AssessmentResults/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            try
            {
                var assessmentResult = await _context.AssessmentResults.FindAsync(id);
                _context.AssessmentResults.Remove(assessmentResult);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(ShowAll));
            }
            catch (Exception)
            {

                return RedirectToAction("Unknown", "Error");
            }

           
        }

        private bool AssessmentResultExists(string id)
        {
            return _context.AssessmentResults.Any(e => e.AssessmentResultID == id);
        }
    }
}
