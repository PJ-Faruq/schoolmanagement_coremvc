﻿using SchoolManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.ViewModel
{
    public class ShowStudentResultViewModel
    {
        public ShowStudentResultViewModel()
        {
            listOfAssessmentResult = new List<AssessmentResult>();
        }
        public string StudentName { get; set; }
        public string Programme { get; set; }
        public  byte[] StudentImage { get; set; }
        public List<AssessmentResult> listOfAssessmentResult;

    }
}
