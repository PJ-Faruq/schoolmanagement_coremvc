﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SchoolManagementSystem.Migrations
{
    public partial class FullName_Added_In_AppUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "ProgrammeID",
                table: "Modules",
                maxLength: 6,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 6,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FullName",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Modules_ProgrammeID",
                table: "Modules",
                column: "ProgrammeID");

            migrationBuilder.AddForeignKey(
                name: "FK_Modules_Programmes_ProgrammeID",
                table: "Modules",
                column: "ProgrammeID",
                principalTable: "Programmes",
                principalColumn: "ProgrammeID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Modules_Programmes_ProgrammeID",
                table: "Modules");

            migrationBuilder.DropIndex(
                name: "IX_Modules_ProgrammeID",
                table: "Modules");

            migrationBuilder.DropColumn(
                name: "FullName",
                table: "AspNetUsers");

            migrationBuilder.AlterColumn<string>(
                name: "ProgrammeID",
                table: "Modules",
                maxLength: 6,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 6);
        }
    }
}
